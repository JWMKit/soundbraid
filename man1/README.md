## SoundBraid

Man pages are the most common form of commanline accessible software documentation found on Linux and other Unix like systems.

| Filename | purpose |
|--|--|
|soundbraid.1  | SoundBraid man page (standard version)  |
| soundbraidl.1 | SoundBraid Limited man page  |

