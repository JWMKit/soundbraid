|  [Versions](#two-versions-avaliable) |   [Advice](#advice-recommendation) | [Features](#features) | [Dependencies](#dependencies) |  [Log](#development-log) | 

## SoundBraid

A Simple audio mixer for SNDIO / ALSA  

SoundBraid is Free Open Source Software.  
See [License](https://codeberg.org/JWMKit/soundbraid/src/branch/main/doc/LICENSE)  

![ ](https://codeberg.org/JWMKit/JWM_Kit/wiki/raw/images/SoundBraid.png  "SoundBraid")

### Two versions avaliable

- **SoundBraid** : The Standard Version. Provides all features.
- **SoundBraid Limited** : SNDIO only. ALSA is not supported.

This repository host both version.  

### Advice / Recommendation
The standard version is recommended over the limited version..  
The Limited version is missing a lot of functionality, and is only for user's who are specifically looking for a SNDIO only mixer.  

### Features

Features with a * are removed from the limited version

- Full control of SNDIO
- Full control of ALSA *
- Dynamic updating of all Controls
- Dynamically add controls for new streams
- enable/disable mute, capture
- select capture devices (mic, line,cd, etc)  *
- select which ALSA controls are shown (or not shown)  *
- customize the display order of ALSA controls  *
- choose if captures controls are displayed before or after playback controls  *
- save control selection and order.  *
- misc controls like mic boost *
- Can run directly from a single file, and from any directory.

### Dependencies
NOTICE : The listed dependencies use Debain package names. These names on maybe different non-Debian based distro.

**Required (All Versions)**  

- python3
- python3-gi
- gir1.2-gtk-3.0
- see additional dependencies for each version below

**standard version** 

- alsa_utils &emsp; *Required*
- sndio &emsp; &nbsp; &emsp; *Recommended* (not required).  Need for sndio support.

**limited version** 

- sndio &emsp; &nbsp; &emsp; *Required*

### Development Log

**Aug. 24, 2024**
- Imporve portablity with an exception to prevent missing assets from stoping execution
- Sucessfuly tested on FreeBSD 

**Dec. 19 2023**

- BUGFIX - added missing quotes around enum items. 
- Option page will not be created  if no options are available
- Option page is now contained in a scroll window as precaution. Just in case of a ridiculous number of switches and other controls.
- If the svg icon is missing try the svg icon from the other version of Soundbraid if it is present.
- To improve non-installed usage, multiple paths are supported for the svg icon. You can use the following.
- - default path : /usr/share/pixmaps/
- - local user icon path:  ~/.local/share/icons/
- - Same directory as the executable : ./
- - Releative git path (running directly for a git clone, or extracted archive..

**Dec. 17 2023**

Added dynamic update function to the check box switches on the options page. Somehow I managed to forget this. It's fix now.

**Dec. 13 2023**

The project is still alive! Wow!  Has it really been 2 years? Life has been busy, sorry for neglecting this project.  

Good news.  You can expect a **new version soon**. You will find the code has already been updated with a lot of changes. Sorry for making so many changes in a single commit. I've been working on it locally and never bothered to push any changes.

Change-log (summed up because I'm not about to iterate all changes)  

- 2 version with and without ALSA support.
- Full ALSA support (levels, mute, capture, input select, mic boost,  etc)
- Bugfixes and improvements  -- that' pretty broad isn't it ;)

**Oct. 26 2021**

- Removed dependency on aucatctl

**Oct. 07 2021**

- Greatly improved dynamic updating of volume levels
- Much smoother operation
- Improved accuracy
- Dynamically add controls for new streams
