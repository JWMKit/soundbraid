## SoundBraid

The freedesktop file (aka dot desktop file) is used for presenting the application to the user. For example by an icon in a menu.


| Filename | purpose |
|--|--|
| soundbraid.desktop  | Free desktop file for SoundBraid (standard version) |
| soundbraidl.desktop | Free desktop file for SoundBraid Limited  |

