## SoundBraid

Additional files required for building a debian package.

| Directory | purpose |
|--|--|
| soundbraid |  SoundBraid (standard version) |
| soundbraidl |SoundBraid Limited  |

