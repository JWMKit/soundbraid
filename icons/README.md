## SoundBraid

This icon is used by the freedesktop file to provide app icons.  It is displayed at the the app.

| Filename | purpose |
|--|--|
| soundbraid.svg  | SoundBraid Icon (standard version)  |
| soundbraidl.svg | SoundBraid Limited Icon  |

