#!/usr/bin/python3
import os, gi, re, sys
from threading import Event, Thread
from shutil import which
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf, GLib


# SoundBraid - Simple audio mixer <https://codeberg.org/JWMKit/soundbraid>
# Copyright © 2021-2023 Calvin Kent McNabb <apps.jwmkit@gmail.com>

# This program is free software:; you can redistribute it and/or modify
# it under the terms of the GNU General Public License, version 2,
# as published by the Free Software Foundation.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.


def get_sndio_state():
    if which('sndioctl') is not None:
        sndioctl = True
    else:
        sndioctl = False
    return sndioctl


def create_shortname(fullname):
    if fullname[-1] == '0':
        return "{}".format(fullname[1:-3], fullname[-1])
    else:
        return "{}{}".format(fullname[1:-3], fullname[-1])


def create_short_sndio(fullname):
    name = fullname[:-6]
    if name.startswith('app/'):
        name = name[4:]
    return name


def get_channel_data(control):
    data = os.popen('amixer get {}'.format(control)).read().strip()
    return '{}\n'.format(data)


def get_switch_state(switch):
    data = get_channel_data(switch[0])
    switch = 'Capture' if switch[1] == 'cswitch' else 'Playback'
    switch = re.findall("\:.+?{} \[(on|off)\]".format(switch), data)[0]
    return switch


def get_vlevels(control, channels):
    data = get_channel_data(control)
    levels = []
    for c in channels:
        levels.extend(re.findall("{}: (?:Playback|Capture|Mono)? ?(\d*)".format(c), data))
    return levels


def get_levels(channels, data):
    levels = []
    for c in channels:
        levels.extend(re.findall("(?s)({}): (?:Playback|Capture|Mono).*?(\d*)%.*?(on|off|\n)".format
                                 (c), data))
    return levels


def request_levels(control, channel):
    data = get_channel_data(control)
    return get_levels(channel, data)


def request_options(control):
    data = os.popen('amixer get {}'.format(control[0])).read().strip()
    data = '{}\n'.format(data)
    try:
        data = re.findall("Item0: '(.*?)'".format(control[0]), data)[0]
    except IndexError:
        data = control[1][0]
    return data


def wrap_name(name, rl):
    length = len(name)
    if length > rl:
        length = int(length/2)
        tmp = name[length:].replace(' ', '\n', 1)
        name = '{}{}'.format(name[:length], tmp)
    return name


def create_slider_name(name, mode):
    if mode == 'sndio':
        name = create_short_sndio(name)
    else:
        name = create_shortname(name)
    return wrap_name(name, 8)


def get_alsa_data():
    streams = os.popen('amixer').read().strip()
    return re.findall("(?s)e mixer control.*?Simpl", '{}Simpl'.format(streams))


def phase_test(stream):
    # Test if stereo channels are swapped.
    # Returns 'Normal', 'Reverse', 'Mono', or 'Not Stereo'
    start_values = os.popen('amixer get "{}"'.format(stream)).read()
    channels = re.findall('(?:Playback|Capture) channels: (.*)', start_values)[0].split(' - ')
    channel_count = len(channels)
    if channel_count < 2:
        return ('Mono')
    if channel_count > 2:
        return('Not Stereo')
    start_values = re.findall('{}: (?:Play|Cap).*?(\d*%)'.format(channels), start_values)
    test_value = 50
    while '{}%'.format(test_value) in start_values:
        test_value += 2
    test_value = os.popen('amixer set "{}" {}%,{}'.format(stream, test_value, start_values[1])).read()
    test_value = re.findall('{}: (?:Play|Cap).*?(\d*%)'.format(channels), test_value)
    new_values = os.popen('amixer get "{}"'.format(stream)).read()
    new_values = re.findall('{}: (?:Play|Cap).*?(\d*%)'.format(channels), new_values)
    if test_value[0] == new_values[0]:
        os.system('amixer -q set "{}" {},{}'.format(stream, start_values[0], start_values[1]))
        return 'Normal'
    else:
        os.system('amixer -q set "{}" {},{}'.format(stream, start_values[1], start_values[0]))
        return 'Reverse'


def read_config(p_controls, c_controls):
    config = '{}/.config/soundbraid/config'.format(os.path.expanduser('~'))
    if os.path.isfile(config):
        with open(config) as f:
            config = f.read()
        revs = re.findall('rev=(.*)', config)
        p_selected = re.findall('p_selected=(.*)', config)
        c_selected = re.findall('c_selected=(.*)', config)
        # remove any non-existing controls from lists
        p_selected = [p for p in p_selected if p in p_controls]
        c_selected = [p for p in c_selected if p in c_controls]
        revs = [p for p in p_selected if p in revs]
        try:
            order = re.findall('order=(.*)', config)[0]
            order = False if order == 'False' else True
        except IndexError:
            order = True
    else:
        return create_defaults(p_controls, c_controls)
    return revs, p_selected, c_selected, order


def write_config(rev, p_selected, c_selected, order):
    path = '{}/.config/soundbraid'.format(os.path.expanduser('~'))
    config = '{}/config'.format(path)
    if not os.path.isdir(path):
        os.makedirs(path)
    data = ''
    for d in rev: data += 'rev={}\n'.format(d)
    for d in p_selected: data += 'p_selected={}\n'.format(d)
    for d in c_selected: data += 'c_selected={}\n'.format(d)
    data += 'order={}\n'.format(order)
    with open(config, 'w+') as f:
        f.write(data)


def create_defaults(p_controls, c_controls):
    # create defaults if no config
    p_selected, c_selected = [], []
    if "'Master',0" in p_controls: p_selected.append("'Master',0")
    if "'PCM',0" in p_controls: p_selected.append("'PCM',0")
    if "'Front',0" in p_controls: p_selected.append("'Front',0")
    if "'Capture',0" in c_controls: c_selected.append("'Capture',0")
    rev = [r for r in p_controls if phase_test(r) == 'Reverse']
    write_config([], p_selected, c_selected, True)
    return rev, p_selected, c_selected, True


def toggle_cmd(button):
    if button.get_relief():
        button.set_relief(Gtk.ReliefStyle.NORMAL)
    else:
        button.set_relief(Gtk.ReliefStyle.NONE)


def create_listbox(controls, listbox):
    for c in controls:
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)
        box.add(Gtk.Label(label=create_shortname(c)))
        listbox.add(box)


class SoundBraid(Gtk.Window):

    def __init__(self):
        sndio_state = get_sndio_state()
        self.current_page = 'Devices'
        icons = ['/usr/share/pixmaps/', '{}/'.format(os.getcwd()), '{}/../icons/'.format(os.getcwd()),
                 '{}/.local/share/icons/'.format(os.path.expanduser('~'))]
        self.sndio_streams, self.locks, self.mutes_buttons, self.p_levels, self.c_levels = [], [], [], [], []
        pcontrols, ccontrols, pswitches, cswitches, pstereo, cstereo, p_channels, c_channels, self.enum, \
            self.v_controls, self.s_switches = self.get_controls()
        self.revs, self.p_select, self.c_select, self.order = read_config(pcontrols, ccontrols)
        p_switches, c_switches, p_stereo, c_stereo = [], [], [], []
        self.p_channels, self.c_channels, self.c_controls, self.p_controls = [], [], [], []
        self.p_count, self.c_count = 0, 0
        self.audio_widgets, self.sndio_widgets, self.option_widgets = [], [], []
        self.v_option_widgets, self.s_option_widgets = [], []

        for p in self.p_select:
            i = pcontrols.index(p)
            p_switches.append(pswitches[i])
            p_stereo.append(pstereo[i])
            self.p_channels.append(p_channels[i])
            self.p_count += len(p_channels[i])
        for p in pcontrols:
            if p not in self.p_select: self.p_controls.append(p)

        for p in self.c_select:
            i = ccontrols.index(p)
            c_switches.append(cswitches[i])
            c_stereo.append(cstereo[i])
            self.c_channels.append(c_channels[i])
            self.c_count += len(c_channels[i])
        for p in ccontrols:
            if p not in self.c_select: self.c_controls.append(p)

        self.pc_count = 0 if not self.order else len(self.p_select)
        self.cc_count = 0 if self.order else len(self.c_select)
        self.p_count = 0 if not self.order else self.p_count
        self.c_count = 0 if self.order else self.c_count

        Gtk.Window.__init__(self, title="SoundBraid")
        icon = self.find_icon(icons)

        self.set_border_width(15)
        main_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        top_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        bottom_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        notebook = Gtk.Notebook()
        self.play_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
        self.select_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)

        self.add(main_box)
        main_box.pack_start(top_box, False, False, 0)
        main_box.pack_end(bottom_box, False, False, 0)
        scroll_box = Gtk.ScrolledWindow()
        scroll_box.set_min_content_height(240)
        scroll_box.add(self.play_box)
        self.play_box.pack_start(Gtk.Separator(orientation=Gtk.Orientation.VERTICAL), True, False, 0)
        notebook.append_page(scroll_box, Gtk.Label(label='Devices'))

        # Create App Page if sndioctl is present
        if sndio_state:
            self.app_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
            scroll_box = Gtk.ScrolledWindow()
            scroll_box.set_min_content_height(240)
            scroll_box.add(self.app_box)
            self.app_box.pack_start(Gtk.Separator(orientation=Gtk.Orientation.VERTICAL), True, False, 0)
            notebook.append_page(scroll_box, Gtk.Label(label='Applications'))

        length = len(self.enum) + len(self.s_switches) + len(self.v_controls)
        if length != 0:
            self.option_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
            scroll_box = Gtk.ScrolledWindow()
            scroll_box.set_min_content_height(240)
            scroll_box.add(self.option_box)
            self.option_v1 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
            self.option_v2 = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
            self.option_box.pack_start(self.option_v1, True, True, 0)
            self.option_box.pack_end(self.option_v2, True, True, 0)
            self.option_box.set_border_width(15)
            notebook.append_page(scroll_box, Gtk.Label(label='Options'))

        notebook.append_page(self.select_box, Gtk.Label(label='View'))
        notebook.connect("switch-page", self.page_name)
        main_box.pack_start(notebook, True, True, 0)

        image = Gtk.Image()
        try:
            pb = GdkPixbuf.Pixbuf.new_from_file_at_scale(icon, 48, 48, preserve_aspect_ratio=True)
            image.set_from_pixbuf(pb)
        except (TypeError, gi.repository.GLib.Error):
            pass
        title_label = Gtk.Label()
        title_label.set_markup('<big>SoundBraid</big>\nSimple sound mixer')
        top_box.pack_start(image, False, False, 0)
        top_box.pack_start(title_label, False, False, 0)
  
        # View Page
        self.listbox = Gtk.ListBox()
        self.listbox.set_selection_mode(Gtk.SelectionMode.SINGLE)
        self.listbox.props.halign = Gtk.Align.CENTER
        self.listbox2 = Gtk.ListBox()
        self.listbox2.set_selection_mode(Gtk.SelectionMode.SINGLE)
        self.listbox2.props.halign = Gtk.Align.CENTER
        self.listbox3 = Gtk.ListBox()
        self.listbox3.set_selection_mode(Gtk.SelectionMode.SINGLE)
        self.listbox3.props.halign = Gtk.Align.CENTER
        self.listbox4 = Gtk.ListBox()
        self.listbox4.set_selection_mode(Gtk.SelectionMode.SINGLE)
        self.listbox4.props.halign = Gtk.Align.CENTER
        self.pc_button = Gtk.Button(label='Playback Controls')
        self.pc_button.set_tooltip_text('Toggle selection of Playback or Capture controls')
        self.pc_button.connect("clicked", self.pc_button_toggle)
        order_check = Gtk.CheckButton(label="Captures after Playback")
        order_check.set_active(self.order)
        order_check.connect("toggled", self.order_toggle)

        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=10)
        box.pack_start(Gtk.Label(), True, False, 0)
        self.select_box.pack_start(box, False, True, 0)
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)
        split_box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=10)
        box.pack_start(Gtk.Label(label="Show"), True, False, 0)
        box.pack_start(self.pc_button, True, True, 0)
        box.pack_end(Gtk.Label(label="Hide"), True, False, 0)
        self.select_box.pack_start(box, False, True, 0)
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)
        box.pack_start(Gtk.Separator(orientation=Gtk.Orientation.HORIZONTAL), True, True, 20)
        self.select_box.pack_start(box, False, True, 0)
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=5)
        box.set_border_width(15)
        self.scrolledwindow = Gtk.ScrolledWindow()
        self.scrolledwindow.set_min_content_height(240)
        self.scrolledwindow2 = Gtk.ScrolledWindow()
        self.scrolledwindow2.set_min_content_height(240)
        self.scrolledwindow3 = Gtk.ScrolledWindow()
        self.scrolledwindow3.set_min_content_height(240)
        self.scrolledwindow4 = Gtk.ScrolledWindow()
        self.scrolledwindow4.set_min_content_height(240)
        self.scrolledwindow.add(self.listbox)
        self.scrolledwindow2.add(self.listbox2)
        self.scrolledwindow3.add(self.listbox3)
        self.scrolledwindow4.add(self.listbox4)
        box.pack_start(Gtk.Separator(orientation=Gtk.Orientation.VERTICAL), False, False, 0)
        box.pack_start(self.scrolledwindow, True, True, 0)
        box.pack_start(self.scrolledwindow3, True, True, 0)
        view_button1 = Gtk.Button(image=Gtk.Image(stock=Gtk.STOCK_GO_FORWARD))
        view_button2 = Gtk.Button(image=Gtk.Image(stock=Gtk.STOCK_GO_BACK))
        up_button = Gtk.Button(image=Gtk.Image(stock=Gtk.STOCK_GO_UP))
        down_button = Gtk.Button(image=Gtk.Image(stock=Gtk.STOCK_GO_DOWN))
        save_button = Gtk.Button(image=Gtk.Image(stock=Gtk.STOCK_SAVE))
        save_button.set_tooltip_text('Save and restart SoundBraid')
        view_button1.connect("clicked", self.move_box1)
        view_button2.connect("clicked", self.move_box2)
        up_button.connect("clicked", self.move_up_action)
        down_button.connect("clicked", self.move_down_action)
        save_button.connect("clicked", self.restart)
        split_box.pack_start(view_button1, False, False, 0)
        split_box.pack_start(view_button2, False, False, 0) 
        split_box.pack_start(up_button, False, False, 0)
        split_box.pack_start(down_button, False, False, 0)
        split_box.pack_end(save_button, False, False, 0)
        box.pack_start(split_box, False, False, 0)
        box.pack_end(Gtk.Separator(orientation=Gtk.Orientation.VERTICAL), False, False, 0)
        box.pack_end(self.scrolledwindow2, True, True, 0)
        box.pack_end(self.scrolledwindow4, True, True, 0)
        self.select_box.pack_start(box, True, True, 0)
        box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
        box.pack_end(order_check, True, False, 5)
        self.select_box.pack_start(box, True, True, 0)

        create_listbox(self.p_select, self.listbox)
        create_listbox(self.p_controls, self.listbox2)
        create_listbox(self.c_select, self.listbox3)
        create_listbox(self.c_controls, self.listbox4)
        self.create_options()

        if self.order:
            self.populate_controls(self.p_select, self.p_channels, p_switches, p_stereo, True)
            self.populate_controls(self.c_select, self.c_channels, c_switches, c_stereo, False)
        else:
            self.populate_controls(self.c_select, self.c_channels, c_switches, c_stereo, False)
            self.populate_controls(self.p_select, self.p_channels, p_switches, p_stereo, True)

        self.show_all()
        self.scrolledwindow3.set_visible(False)
        self.scrolledwindow4.set_visible(False)
        self.call_update()

    def create_options(self, side=True, i=0):
        for e in self.enum:
            label= Gtk.Label(label=create_shortname(e[0]))
            combo = Gtk.ComboBoxText()
            combo.connect('changed', self.option_change, i)
            self.option_widgets.append(combo)
            i += 1
            for item in e[1]:
                combo.append_text(item)
            if side:
                self.option_v1.pack_start(label, False, False, 0)
                self.option_v1.pack_start(combo, False, False, 0)
            else:
                self.option_v2.pack_start(label, False, False, 0)
                self.option_v2.pack_start(combo, False, False, 0)
            side = not side
        self.create_v_options(side)
        self.create_s_options(side)

    def create_s_options(self, side=True, i=0):
        for switch in self.s_switches:
            name = create_shortname(switch[0])
            if name.lower().endswith('enable'):
                name = name[:-6]
            name = wrap_name(name, 22)
            label=Gtk.Label()
            label.set_markup(' <small>{}</small> '.format(name))
            if switch[1] == 'cswitch': name = '{} Capture'.format(name)
            check = Gtk.CheckButton()
            check.connect("toggled", self.switch_toggle, i)
            self.s_option_widgets.append(check)
            i += 1
            box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
            if side:
                box.pack_start(check, False, False, 0)
                box.pack_start(label, False, False, 0)
                self.option_v1.pack_end(box, False, False, 0)
            else:
                label.set_justify(1)
                box.pack_end(check, False, False, 0)
                box.pack_end(label, False, False, 0)
                self.option_v2.pack_end(box, False, False, 0)
            side = not side

    def create_v_options(self, side, i=0):
        def which_side(add_item):
            if side:
                self.option_v1.pack_start(add_item, False, False, 0)
            else:
                self.option_v2.pack_start(add_item, False, False, 0)

        for v in self.v_controls:

            if v[1][0] == 'combo':
                label= Gtk.Label(label=create_shortname(v[0]))
                box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
                which_side(label)
                st = -1 if len(v[2]) == 1 else 0
                for c in v[2]:
                    combo = Gtk.ComboBoxText()
                    self.v_option_widgets.append(combo)
                    combo.connect('changed', self.v_option_change, i, st)
                    st += 1
                    box.pack_start(combo, True, True, 0)

                    for item in range(v[1][1], v[1][2]+1):
                        if item == v[1][1]: item = '{} - off'.format(v[1][1])
                        if item == 1 and item == v[1][2]: item = '{} - on'.format(item)
                        if item == 1: item = '{} - low'.format(item)
                        elif item == v[1][2]: item = '{} - high'.format(item)
                        elif item == int(v[1][2]/2)+1: item = '{} - mid'.format(item)
                        if not v[1][2] % 2:
                            if item == int(v[1][2]/2): item = '{} - mid'.format(item)
                        combo.append_text(str(item))
                i += 1
                which_side(box)
                side = not side

    def find_icon(self, icons):
        names = ['soundbraid', 'soundbraidl']
        for name in names:
            for path in icons:
                try:
                    self.set_icon_from_file('{}{}.svg'.format(path, name))
                    return '{}{}.svg'.format(path, name)
                except gi.repository.GLib.Error:
                    pass

    def order_toggle(self, check):
        self.order = True if check.get_active() else False

    def switch_toggle(self, check, i):
        state = 'on' if check.get_active() else 'off'
        if self.s_switches[i][2] == state:
            return
        self.s_switches[i][2] = state
        if self.s_switches[i][1] == 'pswitch':
            state= state.replace('on', 'unmute').replace('off', 'mute')
        else:
            state= state.replace('off', 'nocap').replace('on', 'cap')
        os.system('amixer -q set "{}" {}'.format(self.s_switches[i][0], state))

    def option_change(self, combo, i):
        a = combo.get_active()
        os.system('amixer -q set "{}" "{}"'.format(self.enum[i][0], self.enum[i][1][a]))

    def v_option_change(self, combo, i, st):
        a = combo.get_active()
        if st == -1:
            os.system('amixer -q set "{}" {}'.format(self.v_controls[i][0], a))
        elif st == 0:
            os.system('amixer -q set "{}" {},0+'.format(self.v_controls[i][0], a))
        else:
            os.system('amixer -q set "{}" 0+,{}'.format(self.v_controls[i][0], a))

    def pc_button_toggle(self, button):
        if button.get_label() == 'Playback Controls':
            button.set_label('Capture  Controls')
            self.scrolledwindow.set_visible(False)
            self.scrolledwindow2.set_visible(False)
            self.scrolledwindow3.set_visible(True)
            self.scrolledwindow4.set_visible(True)
        else:
            button.set_label('Playback Controls')
            self.scrolledwindow.set_visible(True)
            self.scrolledwindow2.set_visible(True)
            self.scrolledwindow3.set_visible(False)
            self.scrolledwindow4.set_visible(False)

    def populate_controls(self, controls, channels, switches, stereo, mute):
        i = 0
        for control in controls:
            self.add_slider((control, channels[i], switches[i], stereo[i], mute), 'alsa', i)
            i += 1

    def move_box1(self, button):
        if self.pc_button.get_label() == 'Playback Controls':
            i = self.listbox.get_selected_rows()[0].get_index()
            selected_row = self.listbox.get_row_at_index(i)
            self.listbox.remove(self.listbox.get_row_at_index(i))
            self.listbox2.insert(selected_row, -1)
            self.listbox2.select_row(None)
            self.p_controls.append(self.p_select[i])
            del self.p_select[i]
        else:
            i = self.listbox3.get_selected_rows()[0].get_index()
            selected_row = self.listbox3.get_row_at_index(i)
            self.listbox3.remove(self.listbox3.get_row_at_index(i))
            self.listbox4.insert(selected_row, -1)
            self.listbox4.select_row(None)
            self.c_controls.append(self.c_select[i])
            del self.c_select[i]

    def move_box2(self, button):
        if self.pc_button.get_label() == 'Playback Controls':
            i = self.listbox2.get_selected_rows()[0].get_index()
            selected_row = self.listbox2.get_row_at_index(i)
            self.listbox2.remove(self.listbox2.get_row_at_index(i))
            self.listbox.insert(selected_row, -1)
            self.listbox.select_row(None)
            self.p_select.append(self.p_controls[i])
            del self.p_controls[i]
        else:
            i = self.listbox4.get_selected_rows()[0].get_index()
            selected_row = self.listbox4.get_row_at_index(i)
            self.listbox4.remove(self.listbox4.get_row_at_index(i))
            self.listbox3.insert(selected_row, -1)
            self.listbox3.select_row(None)
            self.c_select.append(self.c_controls[i])
            del self.c_controls[i]

    def move_up_action(self, widget):
        if self.pc_button.get_label() == 'Playback Controls':
            i = self.listbox.get_selected_rows()[0].get_index()
            if i != 0:
                ii = i - 1
                selected_row = self.listbox.get_row_at_index(i)
                self.listbox.remove(self.listbox.get_row_at_index(i))
                self.listbox.insert(selected_row, ii)
                self.p_select[i], self.p_select[ii] = self.p_select[ii], self.p_select[i]
        else:
            i = self.listbox3.get_selected_rows()[0].get_index()
            if i != 0:
                ii = i - 1
                selected_row = self.listbox3.get_row_at_index(i)
                self.listbox3.remove(self.listbox3.get_row_at_index(i))
                self.listbox3.insert(selected_row, ii)
                self.c_select[i], self.c_select[ii] = self.c_select[ii], self.c_select[i]

    def move_down_action(self, widget):
        if self.pc_button.get_label() == 'Playback Controls':
            i = self.listbox.get_selected_rows()[0].get_index()
            if i is not len(self.p_select) - 1:
                ii = i + 1
                selected_row = self.listbox.get_row_at_index(i)
                self.listbox.remove(self.listbox.get_row_at_index(i))
                self.listbox.insert(selected_row, ii)
                self.p_select[i], self.p_select[ii] = self.p_select[ii], self.p_select[i]
        else:
            i = self.listbox3.get_selected_rows()[0].get_index()
            if i is not len(self.c_select) - 1:
                ii = i + 1
                selected_row = self.listbox3.get_row_at_index(i)
                self.listbox3.remove(self.listbox3.get_row_at_index(i))
                self.listbox3.insert(selected_row, ii)
                self.c_select[i], self.c_select[ii] = self.c_select[ii], self.c_select[i]

    def restart(self, widget):
        write_config(self.revs, self.p_select, self.c_select, self.order)
        os.execv(sys.executable, [os.path.basename(sys.executable)] + sys.argv)

    def channel_lock(self, button, i, si):
        self.locks[i] = not self.locks[i]
        toggle_cmd(button)
        sen = not self.audio_widgets[si].get_visible()
        self.audio_widgets[si].set_visible(sen)
        if sen:
            self.audio_widgets[si].set_value(self.audio_widgets[si-1].get_value())

    def page_name(self, notebook, label, page):
        self.current_page = notebook.get_tab_label(notebook.get_nth_page(page)).get_text()

    def add_slider(self, stream, mode, index):

        def create_mutes(c):
            if c == -1:
                mute_button = Gtk.Button()
                mute_button.set_relief(Gtk.ReliefStyle.NONE)
                mute_button.set_sensitive(False)
            else:
                if switch:
                    mute_button = Gtk.Button(image=Gtk.Image(stock=Gtk.STOCK_CLOSE))
                    mute_button.connect("clicked", self.mute_cmd, mode, real_name, index)
                else:
                    mute_button = Gtk.Button(image=Gtk.Image(stock=Gtk.STOCK_MEDIA_RECORD))
                    mute_button.connect("clicked", self.cap_cmd, mode, real_name, index)

                mute_button.set_always_show_image(True)
                mute_button.set_no_show_all(True)
                mute_button.set_visible(True)
            self.mutes_buttons.append(mute_button)
            if mode == 'sndio':
                mute_button.set_relief(Gtk.ReliefStyle.NORMAL)
            elif s[3] != 'on':
                mute_button.set_relief(Gtk.ReliefStyle.NONE)
            if mode == 'alsa' and c != 0:
                mute_button.set_visible(False)
            button_box.pack_start(mute_button, True, False, 0)

        self.locks.append(True)
        if mode == 'sndio':
            stream = stream.split("=")

        real_name = stream[0]
        name = create_slider_name(real_name, mode)

        box = Gtk.Box(orientation=Gtk.Orientation.VERTICAL, spacing=0)
        label = Gtk.Label(label=name, xalign=.5)
        label.set_markup('<small>{}</small>'.format(name))
        box.pack_start(label, True, False, 0)
        hbox = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
        channel = 0
        button_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
        for s in stream[1]:
            audio_widget = Gtk.Scale.new_with_range(1, 0, 100, .1)
            audio_widget.set_digits(0)
            audio_widget.set_value_pos(Gtk.PositionType.TOP)
            audio_widget.set_inverted(True)
            audio_widget.set_property("height-request", 180)
            audio_widget.set_no_show_all(True)
            audio_widget.set_visible(True)
            hbox.pack_start(audio_widget, True, True, 0)
            if mode == 'alsa':
                audio_widget.connect("value-changed", self.set_value, real_name, mode, index, (channel, stream[3], stream[1]))
                self.audio_widgets.append(audio_widget)
            else:
                audio_widget.connect("value-changed", self.set_value, real_name, mode, index, channel)
                self.sndio_widgets.append(audio_widget)
                audio_widget.set_value(int(float(stream[1])))
                break
            channel += 1

        if mode == 'sndio':
            switch = True
            create_mutes(0)
        elif stream[2]:
            switch = stream[4]
            create_mutes(0)
        else:
            create_mutes(-1)

        lock_box = Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL, spacing=0)
        if mode == 'alsa' and stream[3]:
            lock_button = Gtk.Button(image=Gtk.Image(stock=Gtk.STOCK_DIALOG_AUTHENTICATION))
            lock_button.set_always_show_image(True)
            si = len(self.audio_widgets) - 1
            lock_button.connect("clicked", self.channel_lock, index, si)
            lock_button.set_relief(Gtk.ReliefStyle.NONE)
            audio_widget.set_visible(False)
        else:
            lock_button = Gtk.Button()
            lock_button.set_sensitive(False)
            lock_button.set_relief(Gtk.ReliefStyle.NONE)

        lock_box.pack_end(lock_button, True, True, 0)
        box.pack_end(lock_box, True, True, 0)
        box.pack_end(button_box, False, False, 0)
        box.pack_end(hbox, True, False, 0)
        if mode == 'alsa':
            self.play_box.pack_start(box, True, True, 0)
            self.play_box.pack_start(Gtk.Separator(orientation=Gtk.Orientation.VERTICAL), True, False, 0)
        else:
            self.app_box.pack_start(box, True, True, 0)
            self.app_box.pack_start(Gtk.Separator(orientation=Gtk.Orientation.VERTICAL), True, False, 0)

    def call_update(self):
        action = Event()

        def update():
            while not action.wait(.3):
                if self.current_page == 'Applications':
                    GLib.idle_add(self.update_sndio)
                elif self.current_page == 'Devices':
                    GLib.idle_add(self.update_alsa)
                elif self.current_page == 'Options':
                    GLib.idle_add(self.update_options)

        Thread(target=update, daemon=True).start()
        return

    def set_value(self, slider, name, mode, i, channel):
        level = str(int(slider.get_value()))
        if mode == 'sndio':
            if '{}={}'.format(name, level) not in self.sndio_streams:
                os.system('sndioctl -q {}={}'.format(name, int(level)/100))
        else:
            if channel[1]:
                levels = []
                count = 0
                for s in channel[2]:
                    if channel[0] == count:
                        levels.append('{}%'.format(level))
                    else:
                        if not self.locks[i]:
                            levels.append('0+')
                    count += 1
                # reverse levels for controls with reverse stereo
                if not self.locks[i]:
                    if name in self.revs:
                        levels[0], levels[1] = levels[1], levels[0]

                level = ','.join(levels)
            else:
                level = '{}%'.format(level)
            os.system('amixer -q set "{}" {}'.format(name.strip(), level))

    def update_options(self):
        for ow, option in zip(self.option_widgets, self.enum):
            current = request_options(option)
            ow.set_active(option[1].index(current))
        self.update_v_options()

    def update_v_options(self, i = 0):
        for v in self.v_controls:
            if v[1][0] == 'combo':
                current = get_vlevels(v[0], [v[2]])
                for c in current:
                    self.v_option_widgets[i].set_active(int(c))
                    i+=1
        self.update_s_options()

    def update_s_options(self):
        for s, w in zip(self.s_switches, self.s_option_widgets):
            state = get_switch_state(s)
            if state != s[2]:
                s[2] = state
                state = True if state == 'on' else False
                w.set_active(state)

    def update_sndio(self):
        streams = os.popen('sndioctl').read().strip().split()
        streams = [s for s in streams if '.level=' in s]
        if len(self.sndio_streams) != len(streams):
            self.sndio_streams = streams
            for widget in self.app_box: self.app_box.remove(widget)
            self.sndio_widgets = []
            for i, s in enumerate(streams):
                self.add_slider(s, 'sndio', i)
            self.app_box.show_all()
        else:
            self.sndio_streams = streams

        for widget, stream in zip(self.sndio_widgets, self.sndio_streams):
            stream = stream.split('=')
            if int(float(stream[1]) * 100) != int(widget.get_value()):
                widget.set_value(int(float(stream[1]) * 100))

    def update_alsa(self):

        def update_levels(select, channels):
            levels = []
            for p, c in zip(select, channels):
                levels.append(request_levels(p, c))
            return levels

        def update_controls(index, i):
            for level in levels:
                mute = level[0][2]
                for s in level:
                    if int(s[1]) != int(self.audio_widgets[index].get_value()):
                        self.audio_widgets[index].set_value(int(s[1]))
                    index+=1
                if mute == 'off':
                    self.mutes_buttons[i].set_relief(Gtk.ReliefStyle.NONE)
                elif mute == 'on':
                    self.mutes_buttons[i].set_relief(Gtk.ReliefStyle.NORMAL)
                i += 1

        levels = update_levels(self.p_select, self.p_channels)
        if self.p_levels != levels:
            self.p_levels = levels
            update_controls(self.c_count, self.cc_count)
        levels = update_levels(self.c_select, self.c_channels)
        if self.c_levels != levels:
            self.c_levels = levels
            update_controls(self.p_count, self.pc_count)

    def mute_cmd(self, button, mode, name, i):
        if mode == 'alsa':
            stat = self.p_levels[i][0][2].replace('on', 'mute').replace('off', 'unmute')
            os.system("amixer -q set {} {}".format(name, stat))
            toggle_cmd(button)
        else:
            os.system('sndioctl -q {}=0'.format(name))

    def cap_cmd(self, button, mode, name, i):
        stat = self.c_levels[i][0][2].replace('on', 'nocap').replace('off', 'cap')
        os.system("amixer -q set {} {}".format(name, stat))
        toggle_cmd(button)

    def get_controls(self):
        # p_  = playback | c_ = capture | controls = volume sliders | switches = on/off
        # stereo = True (stereo) False (mono) | channels = name each channel per control
        p_controls, c_controls, v_controls, p_switches, c_switches, p_stereo = [], [], [], [], [], []
        c_stereo, p_channels, c_channels, enum, s_switches = [], [], [], [], []

        alsa_data = get_alsa_data()
        for stream in alsa_data:
            capable = re.findall('Capabilities: (.*)', stream)[0]
            fullname = re.findall("e mixer control (.*)", stream)[0]

            if 'pvolume' in capable and not 'cvolume' in capable:
                p_controls.append(fullname)
                p_switches.append(True) if 'pswitch' in capable else p_switches.append(False)
                p_channel = re.findall("Playback channels:(.*)", stream)[0]
                p_stereo.append(True) if " - " in p_channel else p_stereo.append(False)
                p_channel = p_channel.split(' - ')
                p_channels.append(p_channel)
            elif 'cvolume' in capable:
                c_controls.append(fullname)
                c_switches.append(True) if 'cswitch' in capable else c_switches.append(False)
                c_channel = re.findall("Capture channels:(.*)", stream)[0]
                c_stereo.append(True) if " - " in c_channel else c_stereo.append(False)
                c_channel = c_channel.split(' - ')
                c_channels.append(c_channel)
            elif 'volume' in capable:
                tmp = []
                tmp.append(fullname)
                try:
                    limit = re.findall("Limits:[^\d]+(\d*)[^\d]+(\d*)", stream)[0]
                except IndexError:
                    limit = ['0', '0']
                if int(limit[1]) == 0:
                    tmp.append(['no'])
                elif int(limit[1]) < 9:
                    tmp.append(['combo'])
                else:
                    tmp.append(['slide'])
                tmp[1].append(int(limit[0]))
                tmp[1].append(int(limit[1]))
                channels = re.findall("channels: ?(.*)", stream)[0]
                tmp.append(channels.split(' - '))
                v_controls.append(tmp)
            elif 'switch' in capable:
                if 'pswitch' in capable: 
                    s_switches.append([fullname, 'pswitch'])
                    s_switches[-1].append(get_switch_state(s_switches[-1]))
                if 'cswitch' in capable:
                    s_switches.append([fullname, 'cswitch'])
                    s_switches[-1].append(get_switch_state(s_switches[-1]))
            if 'enum' in capable in capable:
                try:
                    tmp = re.findall("Items: ?(.*)", stream)[0]
                    tmp = re.findall("'([^']*)'", tmp)
                    enum.append((fullname, (tmp)))
                except IndexError:
                    pass

        return p_controls, c_controls, p_switches, c_switches, p_stereo, c_stereo, p_channels, c_channels, enum, v_controls, s_switches


if __name__ == "__main__":
    window = SoundBraid()
    window.connect("delete-event", Gtk.main_quit)
    window.set_position(Gtk.WindowPosition.CENTER)
    Gtk.main()
