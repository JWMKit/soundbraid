## SoundBraid

The program executable written in python. 

It's possible to run the program with only this file, and should run from any directory.  You just need to give it executable permission.

| Filename | purpose |
|--|--|
| soundbraid  | SoundBraid (standard version) |
| soundbraidl | SoundBraid Limited  |

